﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentTest.Domain.Enum
{
    public enum FileType
    {
        CSV,
        XML,
        Unknown
    }
}
