﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AssignmentTest.Domain
{
    public class Transaction : IEntity
    {
        [Key]
        public int Key { set; get; }

        [Required, MaxLength(50), StringLength(50)]
        public string TransactionID { set; get; }
        [Required]
        public DateTime TransactionDate { get; set; }
        [Required]
        public TransactionStatus Status { get; set; }
        public int TransactionPaymentDetailId { get; set; }

        [ForeignKey("TransactionPaymentDetailId")]
        public virtual TransactionPaymentDetail TransactionPaymentDetail { get; set; } = new TransactionPaymentDetail();
    }
}
