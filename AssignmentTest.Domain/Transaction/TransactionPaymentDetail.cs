﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AssignmentTest.Domain
{
    public class TransactionPaymentDetail : IEntity
    {
        [Key]
        public int Key { set; get; }

        [Required]
        [Column(TypeName = "decimal(18,4)")]
        public decimal Amount { set; get; } = 0;
        [Required]
        public string CurrencyCode { set; get; }

    }
}
