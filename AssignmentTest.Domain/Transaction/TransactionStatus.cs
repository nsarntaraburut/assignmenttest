﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentTest.Domain
{
    public enum TransactionStatus
    {
        Approved,
        Rejected,
        Done,
        Finished,
        Failed,
        Unknown
    }
}
