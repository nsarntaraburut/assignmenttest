﻿using AssignmentTest.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentTest.Entity
{
    public class EntitiesContext : DbContext, IEntitiesContext
    {
        public EntitiesContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>()
            .HasOne(p => p.TransactionPaymentDetail)
            .WithOne();

        }
    }
}
