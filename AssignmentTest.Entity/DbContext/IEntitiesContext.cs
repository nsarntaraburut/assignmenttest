﻿using AssignmentTest.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentTest.Entity
{
    public interface IEntitiesContext 
    {
        DbSet<Transaction> Transactions { get; set; }
        DbSet<T> Set<T>() where T : class;
    }
}
