﻿using AssignmentTest.Entity;
using AssignmentTest.Repository;
using AssignmentTest.Repository.TransactionRepository;
using AssignmentTest.Service.Import;
using AssignmentTest.Service.TransactionService;
using Microsoft.Extensions.DependencyInjection;

namespace AssignmentTest.IoC
{
    public class DefaultRegistry
    {
        public static void RegisterIoc(IServiceCollection services)
        {

            //Repository
            services.AddScoped(typeof(IEntityRepository<>), typeof(EntityRepository<>));
            services.AddScoped(typeof(IEntitiesContext), typeof(EntitiesContext));
            services.AddTransient<ITransactionRepository, TransactionRepository>();
 

            //Services
            services.AddTransient<ITransactionService, TransactionService>();
            services.AddTransient<IImportService, ImportService>();
        }

    }
}
