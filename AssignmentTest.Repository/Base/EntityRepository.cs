﻿using AssignmentTest.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssignmentTest.Repository
{
    public class EntityRepository<T> : IEntityRepository<T> where T : class, new()
    {
        private readonly IEntitiesContext _entities;

        public EntityRepository(IEntitiesContext entities)
        {
            _entities = entities;
        }

        public virtual void Insert(T entity)
        {
            _entities.Set<T>().Add(entity);
        }

        public void Insert(IEnumerable<T> entities)
        {
            _entities.Set<T>().AddRange(entities);
        }

        public void Delete(T entity)
        {
            _entities.Set<T>().Remove(entity);
        }

        public void Delete(IEnumerable<T> entities)
        {
            _entities.Set<T>().RemoveRange(entities);
        }

        IQueryable<T> IEntityRepository<T>.GetAll()
        {
            return _entities.Set<T>().AsNoTracking(); ;
        }

        public void Update(T entity)
        {
            _entities.Set<T>().Update(entity);
        }
    }
}
