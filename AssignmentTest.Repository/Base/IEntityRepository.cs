﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssignmentTest.Repository
{
    public interface IEntityRepository<T> where T : class, new()
    {
        void Insert(T entity);
        void Insert(IEnumerable<T> entities);
        void Delete(T entity);
        void Delete(IEnumerable<T> entities);
        void Update(T entity);
        IQueryable<T> GetAll();
    }
}
