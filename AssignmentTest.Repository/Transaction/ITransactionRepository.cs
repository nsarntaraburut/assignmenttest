﻿using System;
using System.Collections.Generic;
using System.Text;
using AssignmentTest.Domain;

namespace AssignmentTest.Repository.TransactionRepository
{
    public interface ITransactionRepository : IEntityRepository<Transaction>
    {

    }
}
