﻿using AssignmentTest.Domain;
using AssignmentTest.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssignmentTest.Repository.TransactionRepository
{
    public class TransactionRepository : EntityRepository<Transaction>, ITransactionRepository
    {
        private readonly IEntitiesContext _entities;
        public TransactionRepository(IEntitiesContext entities)
            : base (entities)
        {
            _entities = entities;
        }


        //This area should implement from a 'custom' repo query regarding its interface ITransactionRepository
    }
}
