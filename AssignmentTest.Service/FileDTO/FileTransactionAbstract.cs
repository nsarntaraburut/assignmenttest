﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AssignmentTest.Service.Model
{
    public abstract class FileTransactionAbstract
    {
        public abstract IList<FileTransactionRecord> Convert(Stream stream);

    }
}
