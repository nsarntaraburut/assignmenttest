﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace AssignmentTest.Service.Model
{
    public class FileTransactionCSV : FileTransactionAbstract
    {
        public override IList<FileTransactionRecord> Convert(Stream stream)
        {
            TextReader textReader = new StreamReader(stream);

            using (var csv = new CsvReader(textReader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.HasHeaderRecord = false;
                var records = csv.GetRecords<FileTransactionRecord>().ToList();
                return records;
            }
        }
    }
}
