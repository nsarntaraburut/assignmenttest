﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace AssignmentTest.Service.Model
{
    public class FileTransactionXML : FileTransactionAbstract
    {
        public override IList<FileTransactionRecord> Convert(Stream stream)
        {
            XmlReader reader = XmlReader.Create(stream);
            XElement xElement = XElement.Load(reader);

            var transactionElements = xElement.Elements().
                Select(s => new FileTransactionRecord
                {
                    TransactionId = s.Attribute("id").Value,
                    Amount = s.Element("PaymentDetails").Element("Amount").Value,
                    CurrencyCode = s.Element("PaymentDetails").Element("CurrencyCode").Value,
                    TransactionDate = s.Element("TransactionDate").Value,
                    Status = s.Element("Status").Value
                }).ToList();

            return transactionElements;
        }
    }
}
