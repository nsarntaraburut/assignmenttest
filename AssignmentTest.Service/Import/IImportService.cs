﻿using AssignmentTest.Domain.Enum;
using AssignmentTest.ViewModel;
using System.IO;

namespace AssignmentTest.Service.Import
{
    public interface IImportService
    {
        void Insert(Stream stream, string fileName);
        ResponseViewModel IsValidFileNameExtension(string fileName);
        ResponseViewModel IsValidFileSize(long fileLenght);
    }
}
