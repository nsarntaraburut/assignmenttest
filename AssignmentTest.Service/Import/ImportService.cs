﻿using AssignmentTest.Domain;
using AssignmentTest.Domain.Enum;
using AssignmentTest.Repository.TransactionRepository;
using AssignmentTest.Service.Model;
using AssignmentTest.Utility;
using AssignmentTest.ViewModel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace AssignmentTest.Service.Import
{
    public class ImportService : IImportService
    {
        ITransactionRepository _transactionRepository { set; get; }
        private readonly long _fileSizeLimitConfig;
        private readonly string[] _permittedExtensionsConfig;
        public ImportService(ITransactionRepository transactionRepository, IConfiguration config)
        {
            _transactionRepository = transactionRepository;
            _fileSizeLimitConfig = config.GetValue<long>("FileSizeLimit");
            _permittedExtensionsConfig = config.GetValue<string>("AllowedFileTypes").Split(',');
        }
        public ResponseViewModel IsValidFileNameExtension(string fileName)
        {
            var response = new ResponseViewModel();
            var ext = Path.GetExtension(fileName).ToLowerInvariant();

            if (!string.IsNullOrEmpty(ext) && _permittedExtensionsConfig.Any(x => x.Equals(ext)))
            {
                response.IsValid = true;
            }
            else
            {
                response.Message = "Unknown format.";
            }
            return response;
        }

        public ResponseViewModel IsValidFileSize(long fileLenght)
        {
            var response = new ResponseViewModel();
            if (fileLenght <= _fileSizeLimitConfig)
            {
                response.IsValid = true;
            }
            else
            {
                response.Message = "File size must less than 1 MB.";
            }
            return response;
        }

        public void Insert(Stream stream, string fileName)
        {
            try
            {
                FileType fileType = FileTypeHelper.Convert(fileName);
                IList<FileTransactionRecord> fileTransactionRecord = ToModel(stream, fileType);
                IEnumerable<Transaction> transactions = ToModel(fileTransactionRecord);
                _transactionRepository.Insert(transactions);

            }
            catch
            {
                throw new Exception("Invalid Input File");
            }


        }

        private static IList<FileTransactionRecord> ToModel(Stream stream, FileType fileType)
        {
            FileTransactionAbstract dataTransform = null;
            if (fileType == FileType.CSV)
            {
                dataTransform = new FileTransactionCSV();
            }
            else if (fileType == FileType.XML)
            {
                dataTransform = new FileTransactionXML();
            }
            return dataTransform.Convert(stream);
        }

        private IEnumerable<Transaction> ToModel(IList<FileTransactionRecord> transaction)
        {
            return transaction.Select(x => new Transaction()
            {
                TransactionID = x.TransactionId,
                Status = TransactionStatusHelper.Convert(x.Status),
                TransactionDate = DateTimeHelper.Convert(x.TransactionDate),
                TransactionPaymentDetail = ToModel(x.Amount, x.CurrencyCode)
            });
        }

        private TransactionPaymentDetail ToModel(string amount, string currencyCode)
        {
            return new TransactionPaymentDetail()
            {
                Amount = Convert.ToDecimal(amount, new CultureInfo("en-US")),
                CurrencyCode = currencyCode

            };
        }
    }
}
