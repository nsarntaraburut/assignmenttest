﻿using AssignmentTest.Domain.Enum;
using AssignmentTest.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AssignmentTest.Service.TransactionService
{
    public interface ITransactionService
    {
        IList<TransactionViewModel> GetTransactions(string currency = "", string status = "", string startDate = "", string endDate = "");
    }
}
