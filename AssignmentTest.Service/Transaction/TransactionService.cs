﻿using AssignmentTest.Domain;
using AssignmentTest.Domain.Enum;
using AssignmentTest.Repository.TransactionRepository;
using AssignmentTest.Service.Model;
using AssignmentTest.Utility;
using AssignmentTest.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AssignmentTest.Service.TransactionService
{
    public class TransactionService : ITransactionService
    {
        public ITransactionRepository _transactionRepository { get; protected set; }
        public TransactionService(ITransactionRepository  transactionRepository)
        {
            _transactionRepository = transactionRepository;

        }

        public IList<TransactionViewModel> GetTransactions(string currency = "", string status = "", string startDate = "", string endDate = "")
        {
            IEnumerable<Transaction> transactions = GetAllTransactions();

            if (!string.IsNullOrEmpty(currency))
            {
                transactions = transactions.Where(t => t.TransactionPaymentDetail.CurrencyCode == currency);
            }

            if (!string.IsNullOrEmpty(status))
            {
                var _status = TransactionStatusHelper.Convert(status);
                transactions = transactions.Where(t => t.Status == _status);
            }

            if (!string.IsNullOrEmpty(startDate))
            {
                var _startDate = DateTimeHelper.Convert(startDate);
                transactions = transactions.Where(t => t.TransactionDate >= _startDate);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                var _endDate = DateTimeHelper.Convert(endDate);
                transactions = transactions.Where(t => t.TransactionDate <= _endDate);
            }
            IList<TransactionViewModel> transactionResponse = transactions.Select(x => new TransactionViewModel(x)).ToList();

            return transactionResponse;
        }

        private IList<Transaction> GetAllTransactions()
        {
            return _transactionRepository.GetAll().ToList();
        }

     
    }
}
