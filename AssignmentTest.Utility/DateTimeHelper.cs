﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace AssignmentTest.Utility
{
    public static class DateTimeHelper
    {
        private const string DATETIME_FORMAT = "dd/MM/yyyy hh:mm:ss";

        public static DateTime Convert(string dateString)
        {
            if (!string.IsNullOrEmpty(dateString))
            {
                return DateTime.ParseExact(dateString, DATETIME_FORMAT, CultureInfo.InvariantCulture);
            }
            else
            {
                return default(DateTime);
            }
        }
    }
}
