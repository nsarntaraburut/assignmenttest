﻿using AssignmentTest.Domain.Enum;
using System.IO;

namespace AssignmentTest.Utility
{
    public class FileTypeHelper
    {
        private const string CSV = ".csv";
        private const string XML = ".xml";
        public static FileType Convert(string fileName)
        {
            var filenameExtension = Path.GetExtension(fileName).ToLowerInvariant();
            FileType fileType = FileType.Unknown;
            switch (filenameExtension)
            {
                case CSV:
                    {
                        fileType = FileType.CSV;
                        break;
                    }
                case XML:
                    {
                        fileType = FileType.XML;
                        break;
                    }
                default:
                    return fileType;

            }
            return fileType;
        }


    }
}
