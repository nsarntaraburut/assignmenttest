﻿using AssignmentTest.Domain;
using System;

namespace AssignmentTest.Utility
{
    public class TransactionStatusHelper
    {

        public static TransactionStatus Convert(string status)
        {
            if (!string.IsNullOrEmpty(status))
            {
                Enum.TryParse(status, out TransactionStatus _status);
                return _status;
            }
            else
            {
                return TransactionStatus.Unknown;

            }
        }

        public static string Convert(TransactionStatus status)
        {
            string reponseStatus;
            switch (status)
            {
                case TransactionStatus.Approved:
                    {
                        reponseStatus = "A";
                        break;
                    }
                case TransactionStatus.Rejected:
                case TransactionStatus.Failed:
                    {
                        reponseStatus = "R";
                        break;
                    }
                case TransactionStatus.Finished:
                case TransactionStatus.Done:
                    {
                        reponseStatus = "D";
                        break;
                    }
                default:
                    return string.Empty;

            }
            return reponseStatus;
        }
    }
}
