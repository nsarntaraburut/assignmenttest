﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentTest.ViewModel
{
    public class ResponseViewModel
    {
        public bool  IsValid { get; set; }
        public string Message { get; set; }

    }
}
