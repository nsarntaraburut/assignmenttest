﻿using AssignmentTest.Domain;
using AssignmentTest.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentTest.ViewModel
{
    public class TransactionViewModel
    {
        public string Id { get; set; }
        public string Payment { get; set; }
        public string Status { get; set; }

        public TransactionViewModel(Transaction transaction)
        {
            this.Id = transaction.TransactionID;
            this.Status = TransactionStatusHelper.Convert(transaction.Status);
            this.Payment = string.Format("{0} {01}", transaction.TransactionPaymentDetail.Amount, transaction.TransactionPaymentDetail.CurrencyCode);

        }

    }
}
