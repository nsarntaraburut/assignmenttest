﻿using System;
using AssignmentTest.Service.Import;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AssignmentTest.Web.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportController : ControllerBase
    {
        private readonly ILogger<ImportController> _logger;
        private readonly IImportService _importService;

        public ImportController(ILogger<ImportController> logger, IImportService importService)
        {
            _logger = logger;
            _importService = importService;
        }

        [HttpPost] //TODO move check validate to MVC filter
        public IActionResult Post(IFormFile file)
        {
            if (file == null)
            {
                return BadRequest("Please select file");
            }

            var isvalidFileNameExtension = _importService.IsValidFileNameExtension(file.FileName);
            if (!isvalidFileNameExtension.IsValid)
            {
                return BadRequest(isvalidFileNameExtension.Message);

            }

            var isvalidFileSize = _importService.IsValidFileSize(file.Length);
            if (!isvalidFileSize.IsValid)
            {
                return BadRequest(isvalidFileNameExtension.Message);
            }

            try
            {
                _importService.Insert(file.OpenReadStream(), file.FileName);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ImportController");
                return BadRequest(ex.Message);
            }

            return Ok(string.Format("{0} Rows was imported", ""));
        }

    }
}