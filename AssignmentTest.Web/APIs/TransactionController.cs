﻿using System;
using System.Collections.Generic;
using AssignmentTest.Service.TransactionService;
using AssignmentTest.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AssignmentTest.Web.APIs
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ILogger<TransactionController> _logger;
        ITransactionService _transactionService;

        public TransactionController(ILogger<TransactionController> logger, ITransactionService transactionService)
        {
            _transactionService = transactionService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<List<TransactionViewModel>> Get(string currency = "", string status = "", string start_date = "", string end_date = "")
        {
            try
            {
                var transactions = _transactionService.GetTransactions(currency, status, start_date, end_date);

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "TransactionController");
                return BadRequest(ex.Message);
            }
        }
    }
}