﻿using AssignmentTest.Repository.TransactionRepository;
using AssignmentTest.Service.Import;
using AssignmentTest.Web.APIs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Http.Results;

namespace AssignmentTest.Service.Test.Controller
{
    public class ImportControllerUnitTest
    {

        ImportController _importController;
        [SetUp]
        public void Setup()
        {

            // Assign:
            var mockImportService = new Mock<IImportService>();
            mockImportService.Setup(x => x.IsValidFileNameExtension(It.IsAny<string>())).Returns(new ViewModel.ResponseViewModel() { IsValid = true, Message = string.Empty });
            mockImportService.Setup(x => x.IsValidFileSize(It.IsAny<long>())).Returns(new ViewModel.ResponseViewModel() { IsValid = true, Message = string.Empty });
            var mockLogger = new Mock<ILogger<ImportController>>();
            _importController = new ImportController(mockLogger.Object, mockImportService.Object);
        }
        [Test]
        public void can_upload_file()
        {
            //Arrange
            var fileMock = new Mock<IFormFile>();

            //Setup mock file using a memory stream
            var content = "Hello World from a Fake File";
            var fileName = "test.csv";
            long filelenght = 500;
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(filelenght);
            var file = fileMock.Object;

            //Act
            var result = _importController.Post(file);

            //Assert
            Assert.IsInstanceOf<OkObjectResult>(result);
        }


        [TestCase(500, "test.pdf")]
        [TestCase(1500, "test.csv")]
        public void can_response_bad_request(long filelenght, string fileName)
        {

            //Arrange
            var fileMock = new Mock<IFormFile>();
            var mockImportService = new Mock<IImportService>();
            //return Invalid
            mockImportService.Setup(x => x.IsValidFileNameExtension(It.IsAny<string>())).Returns(new ViewModel.ResponseViewModel() { IsValid = false, Message = string.Empty }); 
            mockImportService.Setup(x => x.IsValidFileSize(It.IsAny<long>())).Returns(new ViewModel.ResponseViewModel() { IsValid = true, Message = string.Empty });
            var mockLogger = new Mock<ILogger<ImportController>>();
            _importController = new ImportController(mockLogger.Object, mockImportService.Object);

            //Setup mock file using a memory stream

            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(filelenght);
            var file = fileMock.Object;

            //Act
            var result = _importController.Post(file);

            //Assert
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

    }
}
