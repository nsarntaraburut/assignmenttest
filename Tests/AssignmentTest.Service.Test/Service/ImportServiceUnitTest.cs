using AssignmentTest.Domain;
using AssignmentTest.Repository;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using AssignmentTest.Service.Import;
using AssignmentTest.Repository.TransactionRepository;
using Microsoft.Extensions.Configuration;

namespace AssignmentTest.Service.Test
{
    public class ImportServiceUnitTest
    {
        private ImportService _service = null;
        [SetUp]
        public void Setup()
        {
            var mockReprository = new Mock<ITransactionRepository>();
            var mockConfiguratoin = new Mock<IConfiguration>();

            var configurationSection = new Mock<IConfigurationSection>();
            configurationSection.Setup(a => a.Value).Returns(".csv,.xml");

            var configurationSection1 = new Mock<IConfigurationSection>();
            configurationSection1.Setup(a => a.Value).Returns("1048");

            mockConfiguratoin.Setup(a => a.GetSection("FileSizeLimit")).Returns(configurationSection1.Object);
            mockConfiguratoin.Setup(a => a.GetSection("AllowedFileTypes")).Returns(configurationSection.Object);


            var data = new List<Transaction>
            {
                new Transaction { TransactionID = "Inv001" },
                new Transaction { TransactionID = "Inv002" },

            }.AsQueryable();

            mockReprository.Setup(c => c.GetAll()).Returns(data);

            _service = new ImportService(mockReprository.Object, mockConfiguratoin.Object);
      
        }

        [TestCase(".csv", true)]
        [TestCase(".xml", true)]
        [TestCase(".exe", false)]
        public void can_check_file_format(string input, bool expectedResult)
        {
            var result = _service.IsValidFileNameExtension(input);
            Assert.AreEqual(expectedResult, result.IsValid);
        }

        [TestCase(500, true)]
        [TestCase(1048, true)]
        [TestCase(1500, false)]
        public void can_check_file_size(long input, bool expectedResult)
        {
            var result = _service.IsValidFileSize(input);
            Assert.AreEqual(expectedResult, result.IsValid);
        }

        [Test]
        public void can_check_valid_record()
        {
            Assert.Pass();
        }

    }
}