using AssignmentTest.Domain;
using AssignmentTest.Repository.TransactionRepository;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AssignmentTest.Service.Test
{
    public class TransactionServiceUnitTest
    {
        TransactionService.TransactionService _service = null;

        [SetUp]
        public void Setup()
        {
            var mockRepro = new Mock<ITransactionRepository>();
            var data = new List<Transaction>
            {
                new Transaction { TransactionID = "Inv001" },
                new Transaction { TransactionID = "Inv002" },

            }.AsQueryable();

            mockRepro.Setup(c => c.GetAll()).Returns(data);
            _service = new TransactionService.TransactionService(mockRepro.Object);
            var transactions = _service.GetTransactions();

        }


        [Test]
        public void can_get_transaction()
        {
            Assert.Pass(); // TODO

        }
    }
}